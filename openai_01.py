#!/usr/bin/python3
""""""

# standard library
import os
import openai
from icecream import ic
from art import tprint
from pprint import pprint

import requests     # used to make HTTP requests (GET, POST, PUT, DELETE, etc.)

def main():
    """run time code"""

    # from now on all commands are relative to this location
    os.chdir("/home/student/mycode")

    tprint("asking OPENAI a question ...")


    openai.api_key = os.getenv("OPENAI_API_KEY")

    try:
        response = openai.Completion.create(
            model="text-davinci-003",
            prompt="explain REST api",
            temperature=0,
            max_tokens=64,
            top_p=1.0,
            frequency_penalty=0.0,
            presence_penalty=0.0,
            stop=["\"\"\""]
        )
    except (openai.error.RateLimitError) as err:
        raise OSError(f'Something went wrong ... {err}')
    
    # ic(openai.api_key)
    pprint(response)

# best practice to call main()
if __name__ == "__main__":
    main()
