#!/usr/bin/env python3
"""Returning the location of the ISS in latitude/longitude"""
import requests
import datetime
from pprint import pprint
import reverse_geocoder as rg 
    
URL= "http://api.open-notify.org/iss-now.json"
    
def main():
    resp= requests.get(URL).json()
    

    lon= resp["iss_position"]["longitude"]
    lat= resp["iss_position"]["latitude"]
    ts= resp["timestamp"]
    ts = datetime.datetime.fromtimestamp(ts)

    # return an ordered dictionary using our lat/lon vars
    locator_resp= rg.search((lat, lon), verbose=False)
    # pprint(locator_resp) 

    # slice that object to return the city name only
    city= locator_resp[0]["name"]

    # slice the object again to return the country
    country= locator_resp[0]["cc"]

    print(f"""
    CURRENT LOCATION OF THE ISS:
    Timestamp: {ts}
    Lon: {lon}
    Lat: {lat}
    City/Country: {city}, {country}
    """)

    import folium
    from folium.plugins import MarkerCluster
    import pandas as pd

    iss_location = resp["iss_position"]

    my_map = folium.Map(location = iss_location,  # Latitude and Longitude of Map 
                        zoom_start = 13)            # Initial zoom level for the map (default is 10)

    # Add markers to the map
    # popup --> label for the Marker; click on the pins on the map!
    
    folium.Marker(BNYM_Brussels, popup = 'BNYM Brussels').add_to(my_map)
    folium.Marker(BNYM_NY, popup = 'BNYM NY').add_to(my_map)
    folium.Marker(BNYM_NJ, popup = 'BNYM NJ').add_to(my_map)

    folium.Marker(iss_location, popup = 'ISS location').add_to(my_map)

    # change the icon on a marker (location of Hershey Factory)
    # see more icons here: https://getbootstrap.com/docs/3.3/components/
    # folium.Marker([40.285, -76.650], popup = 'Hershey Factory', icon=folium.Icon(color="red", icon="info-sign")).add_to(my_map)

    #Display the map
    my_map


if __name__ == "__main__":
    main()
